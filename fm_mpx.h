/*
    PiFmRds - FM/RDS transmitter for the Raspberry Pi
    Author: Ahmed GHANMI 2016
*/

extern int fm_mpx_open(char *filename, size_t len);
extern int fm_mpx_get_samples(float *mpx_buffer);
extern int fm_mpx_close();
