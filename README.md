# PIFM RDS by Ahmed GHANMI #
This program generates an FM modulation, with RDS (Radio Data System) data generated in real time. It can include monophonic or stereophonic audio.
## How to use it? ##
Clone the source repository and run make in the src directory:

```
git clone https://ahmed_ghanmi@bitbucket.org/fmcast_tm/fmtransmitter.git
make clean
make
```

Then you can just run:

```
sudo ./pi_fm_rds -audio file.wav -freq 100.0 -ps "AhmedRD" -rt "Using Ahmed's FM transmitter as texte message"
```
Or use the pipe file (FIFO) to control RT at runtime.


```
sudo ./pi_fm_rds -audio file.wav -freq 100.0 -ctl ag_ctl
```
